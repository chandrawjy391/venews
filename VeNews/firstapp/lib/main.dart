import 'package:firebase_core/firebase_core.dart';

import 'package:firstapp/pages/Authentication/SplashScreen.dart';

import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'pages/Homepage/MainScreen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        title: 'Login Page',
        theme: ThemeData(
          primaryColor: Colors.white,
          brightness: Brightness.light,
          colorScheme: ColorScheme.fromSwatch(
            accentColor: Color(0XFF2D3D8B),
          ),
        ),
        home: Scaffold(
          body: SplashScreen(),
        ));
  }
}
