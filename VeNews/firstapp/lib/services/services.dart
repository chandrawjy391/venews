import 'dart:async';

import '../model/NewsModel.dart';

import 'package:http/http.dart' as http;
import 'dart:convert';

class Services {
  Future<List<Postmodel>?> getallposts() async {
    try {
      var response = await http
          .get(
        Uri.parse(
            "https://newsapi.org/v2/top-headlines?country=id&apiKey=d924bdc5a55444a4b86593934002a3c3"),
      )
          .timeout(const Duration(seconds: 10), onTimeout: () {
        throw TimeoutException("connection time out try agian");
      });

      if (response.statusCode == 200) {
        Map<String, dynamic> jsonresponse = json.decode(response.body);
        List body = jsonresponse["articles"];

        return body.map((e) => new Postmodel.fromJson(e)).toList();
      } else {
        return null;
      }
    } on TimeoutException catch (_) {
      print("response time out");
    }
  }
}
