import 'dart:convert';

List<Postmodel> postmodelFromJson(String str) =>
    List<Postmodel>.from(json.decode(str).map((x) => Postmodel.fromJson(x)));
String postmodelToJson(List<Postmodel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Postmodel {
  Postmodel(
      {required this.urlToImage,
      required this.author,
      required this.title,
      required this.description,
      required this.url,
      required this.content,
      required this.publishedAt});
  String urlToImage;
  String? author;
  String title;
  String description;
  String url;
  String? content;
  String? publishedAt;
  factory Postmodel.fromJson(Map<String, dynamic> json) => Postmodel(
      urlToImage: json["urlToImage"],
      author: json["author"],
      title: json["title"],
      description: json["description"],
      content: json["content"],
      url: json["url"],
      publishedAt: json["publishedAt"]);
  Map<String, dynamic> toJson() => {
        "urlToImage": urlToImage,
        "author": author,
        "title": title,
        "description": description,
        "url": url,
        "content": content,
        "publishedAt": publishedAt
      };
}
