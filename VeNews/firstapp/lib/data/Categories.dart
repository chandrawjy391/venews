class ChartModel {
  final String name;

  final String imgUrl;

  ChartModel({required this.name, required this.imgUrl});
}

final List<ChartModel> CategoriesItem = [
  ChartModel(
      name: 'Technology',
      imgUrl:
          'https://images.pexels.com/photos/1714208/pexels-photo-1714208.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'),
  ChartModel(
      name: 'Sports',
      imgUrl:
          "https://images.pexels.com/photos/841130/pexels-photo-841130.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"),
  ChartModel(
      name: 'Entertainment',
      imgUrl:
          'https://images.pexels.com/photos/65128/pexels-photo-65128.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'),
  ChartModel(
      name: 'General',
      imgUrl:
          'https://images.pexels.com/photos/5835416/pexels-photo-5835416.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'),
  ChartModel(
      name: 'Health',
      imgUrl:
          'https://images.pexels.com/photos/48604/pexels-photo-48604.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'),
  ChartModel(
      name: 'Science',
      imgUrl:
          'https://images.pexels.com/photos/2280571/pexels-photo-2280571.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'),
  ChartModel(
      name: 'Business',
      imgUrl:
          "https://images.pexels.com/photos/3184292/pexels-photo-3184292.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"),
];
