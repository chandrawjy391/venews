import 'package:firstapp/data/Categories.dart';
import '../../detail/CategoriesDetail.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:get/get.dart';
import '../../controller/NewsController.dart';
import '../../detail/NewsDetail.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    var controller = Get.put(AppController());
    return Scaffold(
        body: Padding(
            padding: EdgeInsets.only(top: 10, left: 10, right: 10),
            child: Column(
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    height: 60,
                    child: ListView.builder(
                        itemCount: CategoriesItem.length,
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) {
                          return GestureDetector(
                              onTap: () {
                                Get.to(CategoriesDetail(
                                    newsCategory: CategoriesItem[index].name));
                              },
                              child: Container(
                                  margin: EdgeInsets.only(right: 3, left: 3),
                                  child: Stack(
                                    children: [
                                      Container(
                                        decoration: BoxDecoration(),
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                          child: Image.network(
                                            CategoriesItem[index].imgUrl,
                                            width: 100,
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                      Container(
                                          alignment: Alignment.center,
                                          width: 100,
                                          height: 60,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              color: Color(0XFF2D3D8B)
                                                  .withOpacity(0.6)),
                                          child: Text(
                                            CategoriesItem[index].name,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold),
                                          )),
                                    ],
                                  )));
                        }),
                  ),
                ),
                SizedBox(height: 10),
                Text(
                  'Berita Trendy',
                  style: GoogleFonts.poppins(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Color(0XFF2D3D8B)),
                ),
                SizedBox(height: 10),
                Expanded(child: Obx(() {
                  return controller.postloading.value
                      ? Center(
                          child: CircularProgressIndicator(),
                        )
                      : Container(
                          height: MediaQuery.of(context).size.height / 1.5,
                          child: ListView.separated(
                              separatorBuilder:
                                  (BuildContext context, int index) {
                                return SizedBox(
                                  height: 20,
                                );
                              },
                              itemCount: controller.getposts.length,
                              itemBuilder: (BuildContext ctx, index) {
                                var item = controller.getposts[index];

                                return GestureDetector(
                                    onTap: () {
                                      Get.to(NewsDetail(
                                        title: item.title,
                                        author: item.author.toString(),
                                        content: item.content.toString(),
                                        image: item.urlToImage,
                                        publishedAt:
                                            item.publishedAt.toString(),
                                      ));
                                    },
                                    child: Container(
                                        padding: EdgeInsets.only(bottom: 20),
                                        decoration: BoxDecoration(
                                            color: Colors.grey[200],
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        alignment: Alignment.center,
                                        child: Padding(
                                            padding: EdgeInsets.symmetric(
                                                vertical: 10, horizontal: 10),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                    child: Image(
                                                      image: NetworkImage(
                                                          item.urlToImage),
                                                    )),
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                Text(item.title,
                                                    style: GoogleFonts.poppins(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 15)),
                                                SizedBox(height: 10),
                                                Text(item.description,
                                                    style: GoogleFonts.poppins(
                                                        color: Colors.black,
                                                        fontSize: 12)),
                                                SizedBox(
                                                  height: 10,
                                                )
                                              ],
                                            ))));
                              }));
                }))
              ],
            )));
  }
}
