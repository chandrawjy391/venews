import 'package:firstapp/pages/Authentication/SplashScreen.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:get/get.dart';
import 'MainScreen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'AboutDeveloper.dart';

class AboutMe extends StatefulWidget {
  const AboutMe({Key? key}) : super(key: key);

  @override
  _AboutMeState createState() => _AboutMeState();
}

class _AboutMeState extends State<AboutMe> {
  @override
  Widget build(BuildContext context) {
    FirebaseAuth auth = FirebaseAuth.instance;
    var name = auth.currentUser!.email.toString().split('@');
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Get.to(MainScreen()),
        ),
        backgroundColor: Color(0XFF2D3D8B),
        title: Text(
          'VeNews',
          style: GoogleFonts.poppins(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: SingleChildScrollView(
          child: Column(children: [
        SizedBox(
          height: 20,
        ),
        CircleAvatar(
          backgroundColor: Colors.blue,
          child: Text(
            name[0][0].toUpperCase(),
            style: GoogleFonts.poppins(fontSize: 50, color: Colors.white),
          ),
          radius: 60,
        ),
        SizedBox(height: 10),
        Text(
          name[0],
          style: GoogleFonts.poppins(
              fontSize: 30,
              color: Color(0XFF2D3D8B),
              fontWeight: FontWeight.bold),
        ),
        SizedBox(
          height: 20,
        ),
        Container(
            height: MediaQuery.of(context).size.height * 0.65,
            decoration: BoxDecoration(
                color: Color(0XFF2D3D8B),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25),
                    topRight: Radius.circular(25))),
            child: Column(children: [
              SizedBox(
                height: 40,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.email,
                    size: 30,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    auth.currentUser!.email.toString(),
                    style: GoogleFonts.poppins(
                        fontSize: 20,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  )
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.perm_identity,
                    size: 30,
                    color: Colors.white,
                  ),
                  Text(
                    "${auth.currentUser!.uid.toString()}",
                    style: GoogleFonts.poppins(
                        fontSize: 15,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  )
                ],
              ),
              SizedBox(
                height: 180,
              ),
              Container(
                height: 50,
                width: 130,
                child: ElevatedButton(
                    onPressed: () {
                      Get.offAll(SplashScreen());
                    },
                    style: ElevatedButton.styleFrom(primary: Colors.white),
                    child: Row(
                      children: [
                        Icon(
                          Icons.logout,
                          color: Color(0XFF2D3D8B),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          'Sign Out',
                          style: GoogleFonts.poppins(
                              fontWeight: FontWeight.bold,
                              color: Color(0XFF2D3D8B)),
                        ),
                      ],
                    )),
              ),
              SizedBox(
                height: 40,
              ),
              InkWell(
                onTap: () {
                  Get.to(AboutDeveloper());
                },
                child: Text(
                  'About Developers',
                  style: GoogleFonts.poppins(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      decoration: TextDecoration.underline),
                ),
              ),
            ])),
      ])),
    );
  }
}
