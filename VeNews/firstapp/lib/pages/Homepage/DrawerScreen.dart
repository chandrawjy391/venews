import 'package:firstapp/pages/Authentication/WelcomeScreen.dart';
import 'package:firstapp/pages/Homepage/AboutMe.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:get/get.dart';
import 'package:firebase_auth/firebase_auth.dart';
import '../../pages/Authentication/SplashScreen.dart';
import 'package:firebase_auth/firebase_auth.dart';

class DrawerScreen extends StatefulWidget {
  const DrawerScreen({Key? key}) : super(key: key);

  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    FirebaseAuth auth = FirebaseAuth.instance;
    var name = auth.currentUser!.email.toString().split('@');
    return Drawer(
        child: ListView(
      children: <Widget>[
        UserAccountsDrawerHeader(
          decoration: BoxDecoration(color: Color(0XFF2D3D8B)),
          accountName: Text(
            name[0],
            style: GoogleFonts.poppins(
                color: Colors.white, fontWeight: FontWeight.bold),
          ),
          currentAccountPicture: CircleAvatar(
            backgroundColor: Colors.blue,
            child: Center(
                child: Text(
              name[0][0].toUpperCase(),
              style: GoogleFonts.poppins(fontSize: 30, color: Colors.white),
            )),
            radius: 30,
          ),
          accountEmail: Text(
            auth.currentUser!.email.toString(),
            style: GoogleFonts.poppins(color: Colors.white, fontSize: 12),
          ),
        ),
        DrawerListTile(
            iconData: Icons.account_circle_sharp,
            title: "Your Profile",
            onTilePressed: () {
              Get.to(AboutMe());
            }),
        DrawerListTile(
            iconData: Icons.logout,
            title: "Sign Out",
            onTilePressed: () {
              Get.offAll(SplashScreen());
            }),
      ],
    ));
  }
}

class DrawerListTile extends StatelessWidget {
  const DrawerListTile(
      {Key? key, this.iconData, this.title, this.onTilePressed})
      : super(key: key);
  final IconData? iconData;
  final String? title;
  final VoidCallback? onTilePressed;
  @override
  Widget build(BuildContext context) {
    return ListTile(
        onTap: onTilePressed,
        dense: true,
        leading: Icon(
          iconData,
          color: Color(0XFF2D3D8B),
        ),
        title: Text(title!,
            style: const TextStyle(fontSize: 15, color: Color(0XFF2D3D8B))));
  }
}
