import 'dart:async';
import 'package:flutter/material.dart';
import 'WelcomeScreen.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:page_transition/page_transition.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
        splash: 'assets/img/VeNews.png',
        splashIconSize: 200,
        backgroundColor: Color(0XFF2D3D8B),
        nextScreen: WelcomeScreen(),
        splashTransition: SplashTransition.fadeTransition,
        pageTransitionType: PageTransitionType.bottomToTop);
  }
}
