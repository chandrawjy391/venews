import 'package:firstapp/main.dart';
import 'LoginScreen.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:get/get.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final GlobalKey<FormState> _form = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  registerSubmit() async {
    try {
      await _firebaseAuth
          .createUserWithEmailAndPassword(
              email: _emailController.text.toString().trim(),
              password: _passwordController.text)
          .then((value) => Get.to(() => LoginScreen()));
      ;
    } catch (e) {
      showDialog(
          context: context,
          builder: (BuildContext context) => AlertDialog(
                backgroundColor: Colors.white,
                title: Text('Sign Up Failed',
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold, color: Color(0XFF2D3D8B))),
                content: Text(
                    e.toString().replaceRange(0, 14, '').split(']')[1],
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold, color: Color(0XFF2D3D8B))),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context, 'Cancel'),
                    child: Text('Try Again',
                        style: GoogleFonts.poppins(
                            fontWeight: FontWeight.bold,
                            color: Color(0XFF2D3D8B))),
                  ),
                ],
              ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: Padding(
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 50),
          child: Center(
            child: Column(
              children: [
                SizedBox(
                  height: 130,
                ),
                Text(
                  'Sign Up',
                  style: GoogleFonts.poppins(
                      fontSize: 50,
                      fontWeight: FontWeight.bold,
                      color: Color(0XFF2D3D8B)),
                ),
                Image.asset(
                  "assets/img/rolling.png",
                ),
                SizedBox(
                  width: 300,
                  height: 70,
                  child: new TextField(
                    controller: _emailController,
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.mail, color: Color(0XFF2D3D8B)),
                        filled: true,
                        fillColor: Colors.white,
                        focusedBorder: OutlineInputBorder(
                          borderSide: const BorderSide(
                              color: Color(0XFF2D3D8B), width: 2.0),
                        ),
                        contentPadding: EdgeInsets.only(left: 30),
                        enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Color(0XFF2D3D8B), width: 2)),
                        labelText: 'New Email',
                        labelStyle:
                            GoogleFonts.poppins(color: Color(0XFF2D3D8B))),
                  ),
                ),
                SizedBox(
                  width: 300,
                  height: 50,
                  child: new TextField(
                    obscureText: true,
                    controller: _passwordController,
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.lock, color: Color(0XFF2D3D8B)),
                      filled: true,
                      fillColor: Colors.white,
                      focusedBorder: OutlineInputBorder(
                        borderSide: const BorderSide(
                            color: Color(0XFF2D3D8B), width: 2.0),
                      ),
                      contentPadding: EdgeInsets.only(left: 30),
                      enabledBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Color(0XFF2D3D8B), width: 2)),
                      labelText: 'New Password',
                      labelStyle: GoogleFonts.poppins(color: Color(0XFF2D3D8B)),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                SizedBox(
                  height: 50,
                  width: 300,
                  child: ElevatedButton(
                    onPressed: () {
                      registerSubmit();
                    },
                    child: Text("Sign Up",
                        style: GoogleFonts.poppins(
                            fontSize: 20, fontWeight: FontWeight.bold)),
                    style: ElevatedButton.styleFrom(
                        primary: Color(0XFF2D3D8B),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30))),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
