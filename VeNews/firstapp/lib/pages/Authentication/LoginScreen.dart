import 'package:firstapp/main.dart';
import 'package:firstapp/pages/Authentication/RegisterPage.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import '../Homepage/MainScreen.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:get/get.dart';
import 'package:firstapp/pages/Homepage/MainScreen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  loginSubmit() async {
    try {
      await _firebaseAuth
          .signInWithEmailAndPassword(
              email: _emailController.text, password: _passwordController.text)
          .then((value) => Get.to(() => MainScreen()));
    } catch (e) {
      showDialog(
          context: context,
          builder: (BuildContext context) => AlertDialog(
                backgroundColor: Colors.white,
                title: Text('Login Failed',
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold, color: Color(0XFF2D3D8B))),
                content: Text(
                    e.toString().replaceRange(0, 14, '').split(']')[1],
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold, color: Color(0XFF2D3D8B))),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context, 'Cancel'),
                    child: Text('Try Again',
                        style: GoogleFonts.poppins(
                            fontWeight: FontWeight.bold,
                            color: Color(0XFF2D3D8B))),
                  ),
                ],
              ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: Padding(
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 40),
          child: Center(
            child: Column(
              children: [
                SizedBox(
                  height: 120,
                ),
                Text(
                  'Login',
                  style: GoogleFonts.poppins(
                      fontSize: 50,
                      fontWeight: FontWeight.bold,
                      color: Color(0XFF2D3D8B)),
                ),
                Stack(children: [
                  Image.asset("assets/img/levitate.png",
                      height: 250, fit: BoxFit.cover),
                  Positioned(
                    bottom: -50,
                    left: 5,
                    child: Container(
                      width: 300,
                      height: 120,
                      margin: EdgeInsets.only(top: 10),
                      child: new TextField(
                        controller: _emailController,
                        decoration: InputDecoration(
                            prefixIcon: Icon(
                              Icons.mail,
                              color: Color(0XFF2D3D8B),
                            ),
                            filled: true,
                            fillColor: Colors.white,
                            focusedBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Color(0XFF2D3D8B), width: 3.0),
                            ),
                            contentPadding: EdgeInsets.only(left: 30),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color(0XFF2D3D8B), width: 2)),
                            labelText: 'Email',
                            labelStyle: GoogleFonts.poppins(
                              color: Color(0XFF2D3D8B),
                            )),
                      ),
                    ),
                  )
                ]),
                SizedBox(
                  width: 300,
                  height: 50,
                  child: new TextField(
                    obscureText: true,
                    controller: _passwordController,
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.lock, color: Color(0XFF2D3D8B)),
                      filled: true,
                      fillColor: Colors.white,
                      focusedBorder: OutlineInputBorder(
                        borderSide: const BorderSide(
                            color: Color(0XFF2D3D8B), width: 3.0),
                      ),
                      contentPadding: EdgeInsets.only(left: 30),
                      enabledBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Color(0XFF2D3D8B), width: 2)),
                      labelText: 'Password',
                      labelStyle: GoogleFonts.poppins(color: Color(0XFF2D3D8B)),
                    ),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                SizedBox(
                  height: 50,
                  width: 290,
                  child: ElevatedButton(
                    onPressed: () {
                      loginSubmit();
                    },
                    child: Text("Login",
                        style: GoogleFonts.poppins(
                            fontSize: 20, fontWeight: FontWeight.bold)),
                    style: ElevatedButton.styleFrom(
                        primary: Color(0XFF2D3D8B),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30))),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Doesn't have an account?",
                        style: GoogleFonts.poppins(
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                          color: Color(0XFF2D3D8B),
                        )),
                    SizedBox(
                      width: 20,
                    ),
                    InkWell(
                      child: Text("Sign Up",
                          style: GoogleFonts.poppins(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: Color(0XFF7FBBCA),
                            decoration: TextDecoration.underline,
                          )),
                      onTap: () {
                        Get.to(RegisterScreen());
                      },
                    )
                  ],
                )
              ],
            ),
          ),
        ));
  }
}
