import 'dart:async';

import 'package:flutter/material.dart';
import '../model/NewsModel.dart';
import '../controller/NewsController.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class NewsDetail extends StatefulWidget {
  final String title;
  final String image;
  final String content;
  final String author;
  final String publishedAt;
  NewsDetail(
      {Key? key,
      required this.title,
      required this.image,
      required this.content,
      required this.author,
      required this.publishedAt})
      : super(key: key);

  @override
  _NewsDetailState createState() =>
      _NewsDetailState(title, image, content, author);
}

class _NewsDetailState extends State<NewsDetail> {
  String? title;
  String? image;
  String? content;
  String? author;
  String? publishedAt;
  _NewsDetailState(title, image, content, author);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Color(0XFF2D3D8B),
          title: Text(
            'VeNews',
            style: GoogleFonts.poppins(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        body: ListView(children: [
          Padding(
              padding: EdgeInsets.only(top: 20, left: 10, right: 10),
              child: Column(
                children: [
                  Text(
                    widget.title,
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.bold, fontSize: 23),
                  ),
                  SizedBox(height: 10),
                  ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image(
                        image: NetworkImage(
                          widget.image,
                        ),
                      )),
                  SizedBox(
                    height: 10,
                  ),
                  Align(
                      alignment: Alignment.centerLeft,
                      child: Text("Ditulis oleh : ${widget.author}",
                          textAlign: TextAlign.left,
                          style: GoogleFonts.poppins(fontSize: 12))),
                  Row(children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Icon(Icons.timer, size: 15),
                    ),
                    Text(widget.publishedAt,
                        style: GoogleFonts.poppins(fontSize: 11))
                  ]),
                  SizedBox(
                    height: 20,
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(widget.content,
                        style: GoogleFonts.poppins(fontSize: 15)),
                  )
                ],
              ))
        ]));
  }
}
