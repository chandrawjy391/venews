import 'package:firstapp/model/NewsModel.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:google_fonts/google_fonts.dart';
import 'package:get/get.dart';
import 'NewsDetail.dart';

class CategoriesDetail extends StatefulWidget {
  final String newsCategory;

  CategoriesDetail({required this.newsCategory});

  @override
  _CategoriesDetailState createState() => _CategoriesDetailState();
}

class _CategoriesDetailState extends State<CategoriesDetail> {
  var newslist;
  bool _loading = true;

  @override
  void initState() {
    getNews();
    // TODO: implement initState
    super.initState();
  }

  void getNews() async {
    NewsForCategorie news = NewsForCategorie();
    await news.getNewsForCategory(widget.newsCategory);
    newslist = news.news;
    setState(() {
      _loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Color(0XFF2D3D8B),
          title: Text(
            'VeNews',
            style: GoogleFonts.poppins(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        body: Stack(children: [
          _loading
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : Container(
                  child: Container(
                    margin: EdgeInsets.only(top: 5),
                    child: ListView.separated(
                        separatorBuilder: (BuildContext context, int index) {
                          return SizedBox(
                            height: 15,
                          );
                        },
                        itemCount: newslist.length,
                        shrinkWrap: true,
                        physics: ClampingScrollPhysics(),
                        itemBuilder: (context, index) {
                          return GestureDetector(
                              onTap: () {
                                Get.to(NewsDetail(
                                  title: newslist[index].title,
                                  author: newslist[index].author.toString(),
                                  content: newslist[index].content.toString(),
                                  image: newslist[index].urlToImage,
                                  publishedAt:
                                      newslist[index].publishedAt.toString(),
                                ));
                              },
                              child: Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Container(
                                      decoration: BoxDecoration(
                                          color: Colors.grey[200],
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      child: Padding(
                                          padding: EdgeInsets.all(10),
                                          child: Column(
                                            children: [
                                              ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                child: Image.network(
                                                    newslist[index]
                                                        .urlToImage
                                                        .toString()),
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Text(newslist[index].title,
                                                  style: GoogleFonts.poppins(
                                                      fontSize: 15,
                                                      fontWeight:
                                                          FontWeight.bold)),
                                              SizedBox(height: 10),
                                              Text(
                                                newslist[index].description,
                                                style: GoogleFonts.poppins(
                                                    fontSize: 12),
                                              ),
                                              SizedBox(
                                                height: 20,
                                              )
                                            ],
                                          )))));
                        }),
                  ),
                ),
          Container(
              width: 200,
              height: 35,
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Color(0XFF2D3D8B).withOpacity(0.5),
                      spreadRadius: 3,
                      blurRadius: 5,
                      offset: Offset(5, 5),
                    ),
                  ],
                  color: Colors.white,
                  borderRadius:
                      BorderRadius.only(bottomRight: Radius.circular(20))),
              child: Align(
                  alignment: Alignment.topCenter,
                  child: Text(
                    widget.newsCategory,
                    style: GoogleFonts.poppins(
                        fontSize: 20,
                        color: Color(0XFF2D3D8B),
                        fontWeight: FontWeight.bold,
                        fontStyle: FontStyle.italic),
                  ))),
        ]));
  }
}

class NewsForCategorie {
  List<Postmodel> news = [];

  Future<void> getNewsForCategory(String category) async {
    var response = await http.get(Uri.parse(
        "https://newsapi.org/v2/top-headlines?country=id&category=$category&apiKey=d924bdc5a55444a4b86593934002a3c3"));

    var jsonData = jsonDecode(response.body);

    if (jsonData['status'] == "ok") {
      jsonData["articles"].forEach((element) {
        if (element['urlToImage'] != null && element['description'] != null) {
          Postmodel article = Postmodel(
              title: element['title'],
              author: element['author'],
              description: element['description'],
              urlToImage: element['urlToImage'],
              url: element["url"],
              publishedAt: element["publishedAt"],
              content: element["content"]);
          news.add(article);
        }
      });
    }
  }
}
